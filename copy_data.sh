#!/bin/bash

BINARY="heartbeat"
GO_PATH="golang/src"
PROJECT_PATH="heartbeat"
BUILD_CONTAINER="registry.gitlab.com/wjm.elbers/alpine-golang-build:1.0.0"

init_data (){
    LOCAL=0
    if [ "$1" == "local" ]; then
        LOCAL=1
    fi

    if [ "${LOCAL}" -eq 0 ]; then
        #Remote / gitlab ci
        echo "Building ${BINARY} remotely"
        cd .. || exit
        docker run --rm -e "BINARY=${BINARY}" -e "PROJECT_PATH=${GO_PATH}" -v "$PWD/${GO_PATH}":/go/src/ -w /go/src/${PROJECT_PATH} ${BUILD_CONTAINER} make
        mv "./${GO_PATH}/${PROJECT_PATH}/${BINARY}"* ./image && \
        cd image || exit
    else
        #Local build
        cd .. || exit
        echo "Building ${BINARY} locally"
	    docker run --rm -e "BINARY=${BINARY}" -e "PROJECT_PATH=${GO_PATH}" -v "$PWD/${GO_PATH}":/go/src/ -w /go/src/${PROJECT_PATH} ${BUILD_CONTAINER} make
        mv "./${GO_PATH}/${PROJECT_PATH}/${BINARY}"* ./image
        cd ./image || exit
    fi
}

cleanup_data () {
    echo "Removing ${BINARY}"
    rm -f "${BINARY}"
}
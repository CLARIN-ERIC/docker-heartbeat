package main

import (
	"bufio"
	"fmt"
	"github.com/spf13/cobra"
	"log"
	"net"
	"os"
	"runtime"
	"strings"
	"time"
)

var rootCmd = &cobra.Command{
	Use:   "heartbeat",
	Short: "Generate heartbeat message a set interval",
	Long:  `Generate heartbeat message a set interval`,
}

const (
	default_iface               = "eth0"
	default_interval            = 300
	layout                      = "2006-01-02 15:04:05.000000 +0000"
	defaultDataRefreshIntervalS = 60
	default_output              = "SIMPLE"
)

func main() {
	var interval int
	var iface string
	var dataRefreshIntervalS int64
	var output string = "SIMPLE"

	var rootCmd = &cobra.Command{
		Use:   "heartbeat",
		Short: "Generate heartbeat message a set interval",
		Long:  `Generate heartbeat message a set interval`,
		Run: func(cmd *cobra.Command, args []string) {
			externalIpv4, internalIpv4, hostname, osName, osVersion, osVersionFull, err := updateData(iface)
			if err != nil {
				log.Fatal(err)
			}

			lastUpdate := time.Now()
			ticker := time.NewTicker(time.Duration(interval) * time.Second)
			printHeartbeat(output, time.Now(), internalIpv4, externalIpv4, hostname, osName, osVersion, osVersionFull)
			for t := range ticker.C {
				printHeartbeat(output, t, internalIpv4, externalIpv4, hostname, osName, osVersion, osVersionFull)

				if int64(time.Since(lastUpdate).Seconds()) > dataRefreshIntervalS {
					externalIpv4, internalIpv4, hostname, osName, osVersion, osVersionFull, err = updateData(iface)
					if err != nil {
						log.Printf("Failed to update data: %s\n", err)
					}
				}
			}
		},
	}
	rootCmd.PersistentFlags().IntVarP(&interval, "interval", "i", default_interval, "Echo interval in seconds.")
	rootCmd.PersistentFlags().StringVarP(&iface, "iface", "f", default_iface, "Internal network interface.")
	rootCmd.PersistentFlags().Int64VarP(&dataRefreshIntervalS, "refresh", "r", defaultDataRefreshIntervalS, "Interval at which to refresh the collected data.")
	rootCmd.PersistentFlags().StringVarP(&output, "output", "o", default_output, "Output format, SIMPLE or FLUENTD.")

	rootCmd.Execute()
}

func printHeartbeat(output string, t time.Time, internalIpv4, externalIpv4, hostname, osName, osVersion, osVersionFull string) {
	switch output {
	case "FLUENTD":
		fmt.Printf("%s\n", getHeartbeatFluentd(t, internalIpv4, externalIpv4, hostname, osName, osVersion, osVersionFull))
	default:
		fmt.Printf("%s\n", getHeartbeatSimple(t, internalIpv4, externalIpv4, hostname, osName, osVersion, osVersionFull))
	}
}

func getHeartbeatSimple(t time.Time, internalIpv4, externalIpv4, hostname, osName, osVersion, osVersionFull string) string {
	return fmt.Sprintf("HEARTBEAT;%s;internal_ipv4=%s;external_ipv4=%s;hostname=%s;os_name=%s;os_version=%s;os_version_full=%s",
		t.Format(layout), internalIpv4, externalIpv4, hostname, osName, osVersion, osVersionFull)
}

func getHeartbeatFluentd(t time.Time, internalIpv4, externalIpv4, hostname, osName, osVersion, osVersionFull string) string {
	return fmt.Sprintf("{\"time\": \"%s\", \"tag\":\"heartbeat\", \"full_message\": \"%s\"}",
		t.Format(layout), getHeartbeatSimple(t, internalIpv4, externalIpv4, hostname, osName, osVersion, osVersionFull))
}

func updateData(iface string) (externalIpv4 string, internalIpv4 string, hostname string, osName string, osVersion string, osVersionFull string, err error) {
	var finalErr error
	externalIpv4, err = getExternalIpAddress()
	if err != nil {
		finalErr = fmt.Errorf("%s,%s", finalErr, err)
	}
	internalIpv4, err = getInternalIpAddresses(iface)
	if err != nil {
		finalErr = fmt.Errorf("%s,%s", finalErr, err)
	}
	hostname, err = os.Hostname()
	if err != nil {
		finalErr = fmt.Errorf("%s,%s", finalErr, err)
	}
	osName, osVersion, osVersionFull = getOs()

	return externalIpv4, internalIpv4, hostname, osName, osVersion, osVersionFull, finalErr
}

// https://stackoverflow.com/questions/23558425/how-do-i-get-the-local-ip-address-in-go
func getExternalIpAddress() (string, error) { //net.IP {
	//Connect over udp, no handshake or connection is needed or established
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		return "", err
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP.String(), nil
}

func getInternalIpAddresses(iface string) (string, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return "", fmt.Errorf("failed to list interfaces: %s", err)
	}

	ipv4 := ""
	ifaceFound := false
	for _, i := range ifaces {
		addrs, err := i.Addrs()
		if err != nil {
			return "", fmt.Errorf("failed to list addresses for interface %s: %s", i.Name, err)
		}

		if i.Name == iface {
			ifaceFound = true
			for _, addr := range addrs {
				var ip net.IP
				switch v := addr.(type) {
				case *net.IPNet:
					ip = v.IP
				case *net.IPAddr:
					ip = v.IP
				}
				// process IP address
				if ip.To4() != nil {
					ipv4 = ip.To4().String()
				}
			}
		}
	}

	if !ifaceFound {
		return "", fmt.Errorf("iface with name = %s not found", iface)
	}
	if ipv4 == "" {
		return "", fmt.Errorf("iface %s does not have an ipv4 address", iface)
	}

	return ipv4, nil
}

func getOs() (osName string, osVersion string, osVersionFull string) {
	if runtime.GOOS != "linux" {
		return runtime.GOOS, "", ""
	}

	filepath := "/host/os-release"
	if _, err := os.Stat(filepath); err == nil {
		file, err := os.Open(filepath)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()

		scanner := bufio.NewScanner(file) // optionally, resize scanner's capacity for lines over 64K, see next example
		for scanner.Scan() {
			lineParts := strings.Split(scanner.Text(), "=")
			if len(lineParts) == 2 {
				switch lineParts[0] {
				case "NAME":
					osName = strings.ReplaceAll(lineParts[1], "\"", "")
				case "VERSION":
					osVersionFull = strings.ReplaceAll(lineParts[1], "\"", "")
				//case "ID":
				case "VERSION_ID":
					osVersion = strings.ReplaceAll(lineParts[1], "\"", "")
				}
			}
		}

		if err := scanner.Err(); err != nil {
			fmt.Printf("%s\n", err)
			return runtime.GOOS, "", ""
		}

		return osName, osVersion, osVersionFull
	}

	return runtime.GOOS, "", ""
}
